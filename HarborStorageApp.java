package de.tuhh.diss.harborstorage;

import java.nio.channels.ShutdownChannelGroupException;
import de.tuhh.diss.harborstorage.HarborStorageManagement;
import de.tuhh.diss.harborstorage.sim.StorageException;
import de.tuhh.diss.io.SimpleIO;

public class HarborStorageApp {

	public static HarborStorageManagement hsm;

	public HarborStorageApp() {
		hsm = new HarborStorageManagement();

	}

	//// SHOW AVAILABLE PACKETS////
	//////////////////////////////
	private static void showAvailablePackets() throws NumberFormatException, StorageException {
		SimpleIO.println("Available packets: ");

		boolean empty = true;
		Packet[] storedPackets = hsm.getPackets();
		for (int i = 0; i < storedPackets.length; i++) {
			if (storedPackets[i] != null) {
				empty = false;
				String description = storedPackets[i].getDescription();
				int width = storedPackets[i].getWidth();
				int height = storedPackets[i].getHeight();
				int depth = storedPackets[i].getDepth();
				int weight = storedPackets[i].getWeight();
				SimpleIO.println(i + 1 + ": Packet '" + description + "' Size: " + width + "x" + height + "x" + depth
						+ " and weight " + weight + ".");
			}
		}
		if (empty == true) {
			SimpleIO.println("There are no stored packets!");
			mainMenu();
		}
	}

	//// MAIN MENU////
	/////////////////
	private static void mainMenu() throws NumberFormatException, StorageException {
		SimpleIO.println("");
		SimpleIO.println("*** Main Menu ***");
		SimpleIO.println("0: Quit program");
		SimpleIO.println("1: Store a packet in the highbaystorage");
		SimpleIO.println("2: Retrieve a packet from the highbaystorage");
		SimpleIO.print("Your choice : ");

		try {
			int response = SimpleIO.readInteger();
			switch (response) {
			case 0:
				SimpleIO.println("");
				SimpleIO.println("System ends. ");
				hsm.shutdown(); // SHUT DOWN PHYSICAL CRANE
				break;
			case 1:
				storePacket();
				break;
			case 2:
				retrievePacket();
				break;
			default:
				SimpleIO.println("Please enter a valid choice! ");
				mainMenu();
			}

		} catch (NumberFormatException e) {
			SimpleIO.println("Please enter a valid choice! ");
			mainMenu();
		}

	}

	////////////////
	/// TRY INPUTS///
	////////////////

	private static int tryWidth() throws NullPointerException, NumberFormatException {

		int width = 0;
		try {
			SimpleIO.print("Width: ");
			width = SimpleIO.readInteger();
		} catch (NumberFormatException n) {
			SimpleIO.println("Please enter a valid width!");
			width = tryWidth();
		} catch (NullPointerException e) {
			SimpleIO.println("Please enter a valid width!");
			width = tryWidth();
		}
		if (width <= 0) {
			SimpleIO.println("Please enter a valid width!");
			width = tryWidth();
		}

		return width;
	}

	private static int tryHeight() throws NullPointerException, NumberFormatException {

		int height = 0;
		try {
			SimpleIO.print("Height: ");
			height = SimpleIO.readInteger();
		} catch (NumberFormatException n) {
			SimpleIO.println("Please enter a valid height!");
			height = tryHeight();
		} catch (NullPointerException e) {
			SimpleIO.println("Please enter a valid height!");
			height = tryHeight();
		}

		if (height <= 0) {
			SimpleIO.println("Please enter a valid height!");

			height = tryHeight();
		}
		return height;
	}

	private static int tryDepth() throws NullPointerException, NumberFormatException {

		int depth = 0;
		try {
			SimpleIO.print("Depth: ");
			depth = SimpleIO.readInteger();
		} catch (NumberFormatException n) {
			SimpleIO.println("Please enter a valid depth!");
			depth = tryDepth();
		} catch (NullPointerException e) {
			SimpleIO.println("Please enter a valid depth!");
			depth = tryDepth();
		}

		if (depth <= 0) {
			SimpleIO.println("Please enter a valid depth!");

			depth = tryDepth();

		}
		return depth;
	}

	private static int tryWeight() throws NullPointerException, NumberFormatException {

		int weight = 0;
		try {
			SimpleIO.print("Weight: ");
			weight = SimpleIO.readInteger();
		} catch (NumberFormatException n) {
			SimpleIO.println("Please enter a valid weight!");
			weight = tryWeight();
		} catch (NullPointerException e) {
			SimpleIO.println("Please enter a valid weight!");
			weight = tryWeight();
		}

		if (weight <= 0) {
			SimpleIO.println("Please enter a valid weight!");

			weight = tryWeight();
		}
		return weight;
	}

	private static String validDescription() {
		SimpleIO.print("Description: ");
		String description = SimpleIO.readString();
		if (description.equals("")) {
			SimpleIO.println("Please enter a valid description!");
			description = validDescription();
		}
		return description;

	}

	//// STORE PACKETS////
	/////////////////////
	private static void storePacket() throws StorageException {
		SimpleIO.println("");
		SimpleIO.println("*** Store a packet ***");
		String chosenPacket = validDescription();
		int width = tryWidth();
		int height = tryHeight();
		int depth = tryDepth();
		int weight = tryWeight();
		SimpleIO.println("You entered packet " + chosenPacket + " of size " + width + "x" + height + "x" + depth
				+ " and weight " + weight + ".");
		SimpleIO.print("Shall we store the packet? (y/n) ");
		String ans = SimpleIO.readString();
		confirmStore(chosenPacket, width, height, depth, weight, ans);

	}

	//// CONFIRM STORE PACKETS////
	/////////////////////////////
	private static void confirmStore(String chosenPacket, int width, int height, int depth, int weight, String ans)
			throws StorageException {
		if (ans.equals("y")) {
			try {
				int id = hsm.storePacket(width, height, depth, chosenPacket, weight);
				SimpleIO.println("Packet stored in rack. The ID is " + id);
				mainMenu();

			} catch (StorageException e) {
				SimpleIO.println("No suitable storage place could be found!");
				SimpleIO.println("Packet cannot be stored!");
				mainMenu();
			}
		} else {
			if (ans.equals("n")) {
				mainMenu();
			} else {
				SimpleIO.print("Please enter a valid answer: (y/n)?");
				String s = SimpleIO.readString();
				confirmStore(chosenPacket, width, height, depth, weight, s);
			}
		}
	}

	//// RETRIEVE PACKETS////
	////////////////////////
	private static void retrievePacket() throws StorageException {
		showAvailablePackets();
		SimpleIO.println("");
		SimpleIO.println("*** Enter a description of packet to be retrieved (0 = Abort) ***");
		String chosenPacket = SimpleIO.readString();
		if (chosenPacket.equals("0")) {
			mainMenu();
		} else {
			try {
				hsm.retrievePacket(chosenPacket);
				mainMenu();
			} catch (StorageException e) {
				SimpleIO.println("Packet not found!");
				retrievePacket();
			}
		}
	}

	//// WELCOME////
	///////////////
	private static void welcome() throws NumberFormatException, StorageException {
		SimpleIO.println("Size: 16x5");
		SimpleIO.println("PhysicalCrane simulator started, v1.2");
		SimpleIO.println("");
		SimpleIO.println("Welcome to TUHH/DISS Harbor Storage Management");
		mainMenu();
	}

	public static void main(String[] args) throws NumberFormatException, StorageException {
		new HarborStorageApp();

		welcome();

	}

}
