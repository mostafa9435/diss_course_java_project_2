package de.tuhh.diss.harborstorage;

import de.tuhh.diss.harborstorage.sim.HighBayStorage;
import de.tuhh.diss.harborstorage.sim.PhysicalCrane;
import de.tuhh.diss.harborstorage.sim.PhysicalHarborStorage;
import de.tuhh.diss.harborstorage.sim.StorageException;
import de.tuhh.diss.harborstorage.sim.StoragePlace;
import de.tuhh.diss.io.SimpleIO;

public class HarborStorageManagement implements HighBayStorage {

	private Packet[] packets;
	private Slot[] slots;
	private PhysicalHarborStorage phs;
	private PhysicalCrane pc;
	private CraneControl cc;

	public HarborStorageManagement() {
		phs = new PhysicalHarborStorage();
		pc = phs.getCrane();
		cc = new CraneControl(pc);
		slots = getSlotsArr(phs.getStoragePlacesAsArray());
		packets = new Packet[slots.length];

	}

	private Slot[] getSlotsArr(StoragePlace[] sp) {
		Slot[] s = new Slot[sp.length];

		for (int i = 0; i < sp.length; i++) {
			s[i] = new Slot();
			s[i].setNumber(sp[i].getNumber());
			s[i].setDepth(sp[i].getDepth());
			s[i].setHeight(sp[i].getHeight());
			s[i].setWidth(sp[i].getWidth());
			s[i].setPositionX(sp[i].getPositionX());
			s[i].setPositionY(sp[i].getPositionY());
			s[i].setLoadCapacity(sp[i].getLoadCapacity());

		}

		return s;
	}

	public int storePacket(int width, int height, int depth, String description, int weight) throws StorageException {
		Packet temp = new Packet(width, height, depth, description, weight);

		// boolean isStored = false;
		int slotIndex = findSuitableSlot(temp);
		int nearestSlotPosX = slots[slotIndex].getPositionX();
		int nearestSlotPosY = slots[slotIndex].getPositionY();
		int id = slots[slotIndex].getNumber() + 1;
		temp.setId(id);
		temp.setLocation(slots[slotIndex]);
		packets[slotIndex] = temp; // update packets[] with the most recent
									// stored packets
		slots[slotIndex].setContainedPacket(packets[slotIndex]);
		cc.storePacket(nearestSlotPosX, nearestSlotPosY, packets[slotIndex]);
		return id;

	}

	public void retrievePacket(String description) throws StorageException {
		for (int i = 0; i < slots.length; i++) {
			if (slots[i].getContainedPacket() != null) {
				if (slots[i].getContainedPacket().getDescription().equals(description)) {
					int slotPosX = slots[i].getPositionX();
					int slotPosY = slots[i].getPositionY();
					cc.retrievePacket(slotPosX, slotPosY);
					slots[i].setContainedPacket(null);
					packets[i] = null;
					return;
				}
			}
		}

		throw new StorageException(null);

	}

	public Packet[] getPackets() {

		int getPacketsLength = 0;
		for (int i = 0; i < packets.length; i++) {
			if (packets[i] != null) {
				getPacketsLength++;
			}
		}
		Packet[] getPackets = new Packet[getPacketsLength];

		int index = 0;
		for (int i = 0; i < packets.length; i++) {
			if (packets[i] != null) {
				getPackets[index] = packets[i];
				index++;
			}
		}
		return getPackets;

	}

	public int findSuitableSlot(Packet P) throws StorageException {

		boolean notSuitable = true;
		int width = P.getWidth();
		int height = P.getHeight();
		int depth = P.getDepth();
		int weight = P.getWeight();
		int index = 0;
		int distance = 10000;
		int nearestDistance = 1000;

		for (int i = 0; i < slots.length; i++) {
			if (width <= slots[i].getWidth() && height <= slots[i].getHeight() && depth <= slots[i].getDepth()
					&& weight <= slots[i].getLoadCapacity()) {
				if (slots[i].getContainedPacket() == null) {
					int slotPosX = slots[i].getPositionX();
					int slotPosY = slots[i].getPositionY();
					distance = slotPosX + slotPosY;
					notSuitable = false;
					if (distance < nearestDistance) {
						nearestDistance = distance;
						index = i;
					}
				}
			}
		}
		if (notSuitable == true)
			throw new StorageException(null);
		return index;

	}

	public void shutdown() {
		cc.shutdown();

	}
}
