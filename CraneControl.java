package de.tuhh.diss.harborstorage;

import de.tuhh.diss.harborstorage.sim.PhysicalCrane;
import de.tuhh.diss.harborstorage.sim.StorageElement;
import de.tuhh.diss.io.SimpleIO;

public class CraneControl {
	private PhysicalCrane cr;

	public CraneControl(PhysicalCrane cr) {
		this.cr = cr;

	}

	public void storePacket(int x, int y, StorageElement packet) {
		int startPosX = cr.getPositionX();
		int startPosY = cr.getPositionY();

		moveToX(cr.getLoadingPosX());
		moveToY(cr.getLoadingPosY());
		cr.loadElement(packet);

		moveToX(x);
		moveToY(y);

		cr.storeElement();

		moveToX(startPosX);
		moveToY(startPosY);

	}

	public StorageElement retrievePacket(int x, int y) {
		moveToX(x);
		moveToY(y);

		cr.retrieveElement();

		moveToX(cr.getLoadingPosX());
		moveToY(cr.getLoadingPosY());

		return cr.unloadElement();
	}

	private void moveToX(int x) {
		while (cr.getPositionX() != x) {
			if (cr.getPositionX() < x) {
				cr.forward();
			}
			if (cr.getPositionX() > x) {
				cr.backward();
			}
		}
		cr.stopX();
	}

	private void moveToY(int y) {
		while (cr.getPositionY() != y) {
			if (cr.getPositionY() < y) {
				cr.up();
			}
			if (cr.getPositionY() > y) {
				cr.down();
			}
		}
		cr.stopY();

	}

	public void shutdown() {
		cr.shutdown();

	}
}
