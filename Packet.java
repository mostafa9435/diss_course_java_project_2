package de.tuhh.diss.harborstorage;

import de.tuhh.diss.harborstorage.sim.StorageElement;

public class Packet implements StorageElement {

	private int id, width, height, depth, weight;
	private String description;
	private Slot location;

	public Packet(int width, int height, int depth, String description, int weight) {
		this.width = width;
		this.height = height;
		this.depth = depth;
		this.description = description;
		this.weight = weight;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getDepth() {
		return depth;
	}

	public int getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public int getWeight() {
		return weight;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Slot getLocation() {
		return location;
	}

	public void setLocation(Slot s) {
		location = s;
	}
}
